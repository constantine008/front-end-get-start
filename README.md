# 📚 开始吧，前端！

从零开始学 Web 系列教程。让我们一起进入前端的学习之旅。

![](https://img.shields.io/badge/stars-129-blue.svg)
![](https://img.shields.io/badge/forks-47-blue.svg)
![](https://img.shields.io/badge/licence-MIT-success.svg)


🎨 Web 前端从入门进阶到高级自学笔记，超详细的前端自学图文教程。

> 建议 Chrome 用户下载 `Octotree` 或者 `GayHub` 插件，查看目录文件更方便哦~

另外还有一个微信公众号「[前端队长](https://github.com/Daotin/pic/raw/master/wx.jpg)」，佛系更新...(*/ω＼*)


